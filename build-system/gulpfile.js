const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

const source = 'src'; //?
const dest = source,
    css = {
        in: source + '/scss/main.scss', //?
        watch: [source + '/scss/**/*'],
        out: dest,
        sassOpts: {
            outputStyle: 'nested',
            imagePath: '../img',
            precision: 3,
            errLogToConsole: true
        }
    };

//  Sass
// const buildStyles = function buildStyles() {
//     return gulp.src(css.in)
//         .pipe(sass().on('error', sass.logError))
//         .pipe(gulp.dest(css.out));
// };

const buildStyles = function() {
    return gulp.src(css.in)
        .pipe(sass())
        .pipe(gulp.dest(css.out))
        .pipe(browserSync.stream());
}


// Browser-sync
const bspFunc =
    function bspFunc() {
        browserSync.init({
            server: {
                baseDir: source
            },
            port: 3020,
            ui: {
                port: 4040
            }
        });
        gulp.watch(source + '/scss/*.scss', buildStyles);
        // gulp.watch(source + 'scss/**/*.scss',buildStyles).on('change', reload);
        gulp.watch(source+"/main.css").on("change", reload);
        gulp.watch("**/*").on("change", reload);
        // gulp.watch(source + 'scss/**/*.scss').on('change', sassFunc);
    };

gulp.task('default', gulp.parallel(
    gulp.series(bspFunc),
));


exports.buildStyles = buildStyles;
exports.bspFunc = bspFunc;
exports.default = 'default';
